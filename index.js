/**
 * Created by Vadim on 18.11.2016.
 */
var express = require('express')
var app = express()

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    var regex = new RegExp('@?(https?:)?(\/\/)?(www.)?((telegram|vk|vkontakte|twitter|github|xn--80adtgbbrh1bc|medium)[^\/]*\/)?(@)?([a-zA-Z0-9\._]*)','i');
    var username = req.query.username.match(regex);

    if (username[7].length > 1 && username[7] != "@") {
        console.log(username);
        res.send("@" + username[7]);
    }
    else {
        console.log(username);
        res.send("Invalid username");
    }
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});